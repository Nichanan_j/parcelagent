import 'package:progress_hud/progress_hud.dart';
import 'package:flutter/material.dart';

var progress = new ProgressHUD(
  backgroundColor: Colors.black12,
  color: Colors.white,
  containerColor: Colors.blue,
  borderRadius: 5.0,
  text: "Loading...",
);