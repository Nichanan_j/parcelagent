import 'dart:async';

import 'package:flutter/material.dart';

import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:new_s_t/Home/index.dart';
import 'package:new_s_t/Login/LoginPage.dart';
import 'package:new_s_t/MasterPage/headder.dart';
import 'package:new_s_t/Report/UiReport.dart';
import 'package:shared_preferences/shared_preferences.dart';

const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

//String selectedUrl = 'https://mb.945.report/all-parcel/?wdt_column_filter[phoneregis]=';
String selectedUrl = 'https://mb.945.report/?wdt_column_filter[phoneregis]=';
String phone;
void main() {
  runApp(new MyAppHomePage());
}
class MyAppLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MY945.ME',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),

      home: new LoginPage(),
    );
  }
}
class MyAppHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'MY945.ME',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        routes: {
          '/': (_) => const MyHomePage(title: 'MY945.ME'),
          '/widget': (_) => new WebviewScaffold(
            url: selectedUrl+"66"+phone.substring(1,phone.length),
            appBar: new AppBar(
              title: const Text('COD REPORT'),
            ),
            withZoom: true,
            withLocalStorage: true,
          ),
          '/my945': (_) => new WebviewScaffold(
            url: 'https://www.945holding.com',
            appBar: new AppBar(
              title: const Text('945holding'),
            ),
            withZoom: true,
            withLocalStorage: true,
          ) ,
         '/bubbleapps': (_) => new WebviewScaffold(
            url: 'https://pc-cover.bubbleapps.io',
            appBar: new AppBar(
              title: const Text('Info'),
            ),
            withZoom: true,
            withLocalStorage: true,
          )
        },
      );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Instance of WebView plugin
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  // On destroy stream
  StreamSubscription _onDestroy;

  // On urlChanged stream
  StreamSubscription<String> _onUrlChanged;

  // On urlChanged stream
  StreamSubscription<WebViewStateChanged> _onStateChanged;

  StreamSubscription<WebViewHttpError> _onHttpError;

  StreamSubscription<double> _onScrollYChanged;

  StreamSubscription<double> _onScrollXChanged;

  final _urlCtrl = new TextEditingController(text: selectedUrl);

  final _codeCtrl =
  new TextEditingController(text: 'window.navigator.userAgent');

  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _history = [];

  @override
  void initState() {
    super.initState();
    getCredential1();
    flutterWebviewPlugin.close();

    _urlCtrl.addListener(() {
      selectedUrl = _urlCtrl.text;
    });

    // Add a listener to on destroy WebView, so you can make came actions.
    _onDestroy = flutterWebviewPlugin.onDestroy.listen((_) {
      if (mounted) {
        // Actions like show a info toast.
        _scaffoldKey.currentState.showSnackBar(
            const SnackBar(content: const Text('MY945.ME')));
      }
    });

    // Add a listener to on url changed
    _onUrlChanged = flutterWebviewPlugin.onUrlChanged.listen((String url) {
      if (mounted) {
        setState(() {
          _history.add('onUrlChanged: $url');
        });
      }
    });

    _onScrollYChanged =
        flutterWebviewPlugin.onScrollYChanged.listen((double y) {
          if (mounted) {
            setState(() {
              _history.add("Scroll in  Y Direction: $y");
            });
          }
        });

    _onScrollXChanged =
        flutterWebviewPlugin.onScrollXChanged.listen((double x) {
          if (mounted) {
            setState(() {
              _history.add("Scroll in  X Direction: $x");
            });
          }
        });

    _onStateChanged =
        flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged state) {
          if (mounted) {
            setState(() {
              _history.add('onStateChanged: ${state.type} ${state.url}');
            });
          }
        });

    _onHttpError =
        flutterWebviewPlugin.onHttpError.listen((WebViewHttpError error) {
          if (mounted) {
            setState(() {
              _history.add('onHttpError: ${error.code} ${error.url}');
            });
          }
        });
  }
  getCredential1() async {

    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
     phone = sharedPreferences.getString("phonenumber");
      //getUserDetails("0863769051");
      print(phone.substring(1,phone.length));

    });
  }
  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.
    _onDestroy.cancel();
    _onUrlChanged.cancel();
    _onStateChanged.cancel();
    _onHttpError.cancel();
    _onScrollXChanged.cancel();
    _onScrollYChanged.cancel();

    flutterWebviewPlugin.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: buildAppBar(context),
      body:(

          new Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/bg01.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child:  new Column(

                //mainAxisAlignment: MainAxisAlignment.center,
               // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(

                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(

                            padding: const EdgeInsets.only(bottom:  10.0,right: 5.0,top: 30.0,left: 5),
                          child: FlatButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => MyAppHomePageReport()),
                              );
                            },
                            //color: Colors.lightBlue,
                            padding: EdgeInsets.all(0.0),

                            child: Column( // Replace with a Row for horizontal icon + text
                              children: <Widget>[

                                Image(
                                  image: AssetImage('assets/appp01.jpg'),
                                  width: 150.0,
                                  fit: BoxFit.fill,
                                ),

                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom:  10.0,left: 11.0,top: 30.0),
                          child: FlatButton(
                            onPressed: () {

                              Navigator.of(context).pushNamed('/widget');
                              //Navigator.of(context).pushNamed('/widget');
                              //MyAppHomePageReport
                            },
                            //color: Colors.lightBlue,
                            padding: EdgeInsets.all(0.0),
                            child: Column( // Replace with a Row for horizontal icon + text
                              children: <Widget>[
                                Image(
                                  image: AssetImage('assets/appp02.jpg'),
                                  width: 150.0,

                                  fit: BoxFit.fill,
                                ),

                              ],
                            ),
                          ),
                        ),
                      ]
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom:  0.0,left: 0.0,top: 10),
                          child: FlatButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePageIndex()));
                            },
                            //color: Colors.lightBlue,
                            padding: EdgeInsets.only(left: 19,top: 3),

                            child: Column( // Replace with a Row for horizontal icon + text
                              children: <Widget>[

                                Image(
                                  image: AssetImage('assets/appp04.jpg'),
                                  width: 150.0,
                                  //height: ma,
                                  fit: BoxFit.fill,
                                ),

                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom:  0.0,left: 0.0,top: 12),
                            child: Column( // Replace with a Row for horizontal icon + text
                              children: <Widget>[
                            Container(
                                child: FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pushNamed('/bubbleapps');
                                  },
                                  child:  Image(
                                    image: AssetImage('assets/appp033.jpg'),
                                    width: 150,
                                    fit: BoxFit.fill,
                                  ),
                                ),

                            ),

                           Container(
                              padding: new EdgeInsets.only(top: 22.0),
                              child: FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pushNamed('/my945');
                                },
                                child:  Image(
                                  image: AssetImage('assets/appp05.jpg'),
                                  width: 150,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              ),


                              ],
                            ),

                        ),
                      ]
                  ),

                ]
            )
          ) /* add child content content here */
          ),

    );
  }

  Padding buildPaddingListView(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: ListView(

        children: <Widget>[
          Padding(

            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyAppHomePageReport()),
                );
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 40.0,right: 40.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(Icons.dashboard,color: Colors.white,size: 50.0,),
                  Text(
                    "Dashboard",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/widget');
                //Navigator.of(context).pushNamed('/widget');
                //MyAppHomePageReport
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 27.0,right: 27.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[

                  Icon(Icons.report,color: Colors.white,size: 50.0,),
                  Text(
                    "COD REPORT",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePageIndex()),
                );
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 40.0,right: 40.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(Icons.home,color: Colors.white,size: 50.0,),
                  Text(
                    "MY945.ME",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Column buildCard(BuildContext context) {
    return Column(

        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[
          Padding(

            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/widget');
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 40.0,right: 40.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(Icons.dashboard,color: Colors.white,size: 50.0,),
                  Text(
                    "Dashboard",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyAppHomePageReport()),
                );
                //Navigator.of(context).pushNamed('/widget');
                //MyAppHomePageReport
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 27.0,right: 27.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(Icons.report,color: Colors.white,size: 50.0,),
                  Text(
                    "COD REPORT",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FlatButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyHomePageIndex()),
                );
              },
              color: Colors.lightBlue,
              padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 40.0,right: 40.0),
              child: Column( // Replace with a Row for horizontal icon + text
                children: <Widget>[
                  Icon(Icons.home,color: Colors.white,size: 50.0,),
                  Text(
                    "MY945.ME",
                    style: new TextStyle(
                      fontSize: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),


        ],

    );
  }

  Column buildColumn(BuildContext context) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [

        Padding(

          padding: const EdgeInsets.only(left :50.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/widget');
                  },
                  color: Colors.lightBlue,
                  padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 50.0,right: 50.0),
                  child: Column( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.dashboard,color: Colors.white,size: 80.0,),
                      Text(
                        "Dashboard",
                        style: new TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyAppHomePageReport()),
                    );
                    //Navigator.of(context).pushNamed('/widget');
                    //MyAppHomePageReport
                  },
                  color: Colors.lightBlue,
                  padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 33.0,right: 33.0),
                  child: Column( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.report,color: Colors.white,size: 80.0,),
                      Text(
                        "COD REPORT",
                        style: new TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyHomePageIndex()),
                    );
                  },
                  color: Colors.lightBlue,
                  padding: EdgeInsets.only(top:20.0,bottom: 20.0,left: 50.0,right: 50.0),
                  child: Column( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Icon(Icons.home,color: Colors.white,size: 80.0,),
                      Text(
                        "MY945.ME",
                        style: new TextStyle(
                          fontSize: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),


            ],
          ),
        ),



      ],
    );
  }
}