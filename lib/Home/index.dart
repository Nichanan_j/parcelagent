import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:new_s_t/Home/Ui.dart';
import 'package:new_s_t/MasterPage/headder.dart';
import 'package:new_s_t/Report/RePortStatus.dart';
import 'package:new_s_t/Report/UiReport.dart';
import 'package:new_s_t/System/Loading.dart';
import 'package:new_s_t/User/UserHistory.dart';
import 'package:new_s_t/User/UserHistoryMain.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_hud/progress_hud.dart';

import 'dart:async';
import 'package:flutter/services.dart';
// import 'package:barcode_scan/barcode_scan.dart';
import 'package:new_s_t/System/apiPost.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:intl/intl.dart';

String myController = "";
final myController1 = TextEditingController();



class MyHomePageIndex extends StatefulWidget {
  MyHomePageIndex({Key key, this.title}) : super(key: key);
  final String title;


  @override
  _MyHomePageState createState() => new _MyHomePageState();

}

class _MyHomePageState extends State<MyHomePageIndex> {
  String barcode = "";
  final myController = TextEditingController();
  SharedPreferences sharedPreferences;
  String user = "";
  ProgressHUD _progressHUD;
  String jsonnn = "";
  String dataPara = "";
  String dataSetImage = "";
  String phone = "";
  var responseJson;
  int _count = 0;
  String dataTitle = "";
  bool _loading = true;
  @override
  void initState() {
    super.initState();
    //getCredential();
    getCredential1();
    //phone = sharedPreferences.getString("phonenumber");

  }
  getCredential1() async {

    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _userDetails.clear();
      phone = sharedPreferences.getString("phonenumber");
      //getUserDetails("0863769051");
     // getUserDetails("0951825674");
      getUserDetails(phone);
      //print("12121212"+phone);
    });
  }
  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    myController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    int _currentIndex = 0;
    final List<Widget> _children = [];
    return new Scaffold(
      appBar: buildAppBarNotLogout(context),
      //bottomNavigationBar: buildBottomNavigationBar(onTabTapped,_currentIndex),
      body: _body(),

    );

  }

  final String url = 'https://www.945holding.com/webservice/restful/parcel/tracking/v11/get_billing';
  Future<Null> getUserDetails(String tack) async {
    showDialog(
        context: context,
        child: progress);

    final response = await http.post(url,body: {"phoneno": tack},headers: {"apikey":apikey});
    //Map<String, dynamic> user = json.decode(response.body);
    //var streetsFromJson = parsedJson['streets'];
    //print("js=>>>>>>>>"+user["billing_info"]);
    //Map decoded = jsonDecode(response.body);
    String testJson = "[{'billing_no':'2-1-181004180705-439','billing_summary':'35.00','billing_date':'2018-10-04 18:07:08'}]";
    //String name = decoded['billing_info'];
    print("js=>>>>>>>>"+response.body);


    //"file_fur_point_id": "20088",
    try {
      var s1 = response.body.split("[");
      var s2 = "["+s1[2].toString().replaceAll("}]}]", "}]");
      jsonnn = s2;
      print("js2>>>>>>"+jsonnn.replaceAll(",", "\n"));
      responseJson = json.decode(jsonnn);
      print(responseJson);
      setState(() {
        for (Map user in responseJson) {
          _userDetails.add(UserDetails.fromJson(user));
        }
      });
      print("js2>>>>>>"+_userDetails[0].billing_no);
      Navigator.of(context, rootNavigator: true).pop('dialog');
    }catch(e){
      Navigator.of(context, rootNavigator: true).pop('dialog');
    }
    //dataTitle
    //Navigator.pop(context);
  }
  //_userCount
  Widget _body() {
    return new Container(
        color: Colors.white.withOpacity(1.0),
        padding: EdgeInsets.only(right: 0.0, left: 0.0),
        child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[


        new Container(
        //color: Theme.of(context).primaryColor,

      child: new Padding(
        padding: const EdgeInsets.only(top:  7.0,bottom: 5.0,left: 10.0,right: 10.0),

        child: new Container(
          //padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 2.0),
          decoration: new BoxDecoration(
              border: new Border.all(color: Colors.grey)
          ),
          child: new ListTile(
            //leading: new Icon(Icons.search),
            title: new TextField(

              controller: myController,
              decoration: new InputDecoration(

                  hintText: "ติดตามพัสดุ", border: InputBorder.none),
              // onChanged: onSearchTextChanged,
            ),
            trailing: new IconButton(icon:  new Image.network("https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fif_qr_code_1613767.png?alt=media&token=43037295-df25-4903-9ca9-a10528f964b8"),
              onPressed:_onChanged,
            ),
          ),
        ),
      ),
    ),

        new Expanded(

            child: Container(

              padding: const EdgeInsets.all(5.0),
              child: new ListView.builder
                (
                  itemCount: _userDetails.length,
                  itemBuilder: (BuildContext ctxt, int Index) {
                    return new Container(

                      child: GestureDetector(
                        onTap: () {
                          _onChanged1(_userDetails[Index].billing_no);
                        },
                        child: Container(

                          padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                          child: new Container(
                            padding: const EdgeInsets.only(bottom: 2.0),
                            color: Colors.lightBlue.withOpacity(0.8),
                            child: new Container(
                              padding: const EdgeInsets.all(1.0),
                              color: Colors.white.withOpacity(1.0),

                              child: Row(
                                children: <Widget>[

                                  new Center(
                                    child: new InkWell(// this is the one you are looking for..........
                                      onTap: () => setState(() => _count++),
                                      child: new Container(
                                        //width: 50.0,
                                        //height: 50.0,
                                        padding: const EdgeInsets.all(20.0),//I used some padding without fixed width and height
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,// You can use like this way or like the below line
                                          //borderRadius: new BorderRadius.circular(30.0),
                                          color: Colors.lightBlue,
                                        ),
                                        //getRunnumber(Index +1)
                                        child: new Text(getRunnumber(_userDetails[Index].count_tracking), style: new TextStyle(color: Colors.white, fontSize: 16.0)),// You can add a Icon instead of text also, like below.
                                        //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                      ),//............
                                    ),
                                  ),

                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[

                                      Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisSize: MainAxisSize.max,

                                          children: <Widget>[

                                            Container(
                                              padding: const EdgeInsets.only(top: 0.0,left: 20.0,bottom: 8.0),
                                              child: new Text(getDataTime(_userDetails[Index].billing_date),
                                                  style: new TextStyle(fontSize: 22.0 ,color: Colors.blueAccent.withOpacity(1.0)),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(top: 4.0,left: 0.0,bottom: 8.0),
                                              child: new Text(" | ",
                                                  style: new TextStyle(fontSize: 22.0 ,color: Colors.orange.withOpacity(1.0)),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis),
                                            ),
                                            Container(
                                              padding: const EdgeInsets.only(top: 7.0,left: 0.0,bottom: 8.0),
                                              child: new Text(getRowWPice(_userDetails[Index].billing_summary),textAlign: TextAlign.right,
                                                  style: new TextStyle(fontSize: 22.0 ,color: Colors.blueAccent.withOpacity(1.0)),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis),
                                            ),
                                            ]
//
                                      ),
//

                                       Row(
                                           crossAxisAlignment: CrossAxisAlignment.end,
                                           mainAxisSize: MainAxisSize.max,
                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                           children: <Widget>[
                                             Container(
                                               padding: const EdgeInsets.only(left: 15.0,bottom: 0.0),
                                               child: new Text(getRowW(_userDetails[Index].billing_no),
                                                   style: new TextStyle(fontSize: 16.0 ,color: Colors.grey.withOpacity(1.0)),
                                                   maxLines: 1,textAlign: TextAlign.right,
                                                   overflow: TextOverflow.ellipsis),
                                             ),
                                             ]
                                   )


//
                                    ],
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ),

                      ),
                    );
                  }
              ),
            )
        )

            ]

     ),

    );
  }
  getRowW(String number) {
    String numShow ;
     if(number.length == 20){
      numShow = "         เลขบิล "+number;
    }else if(number.length == 21){
      numShow = "       เลขบิล "+number;
    }else if(number.length == 22){
      numShow = "   เลขบิล "+number;
    }else if(number.length == 23){
      numShow = "  เลขบิล "+number;
     }else if(number.length == 24){
      numShow = " เลขบิล "+number;
     }else if(number.length == 25){
      numShow = "เลขบิล "+number;
     }else{
      numShow = "เลขบิล "+number;
    }

    return numShow;
  }
  getRowWPice(String numberSet) {
    print("ssssssssssss"+numberSet);
    final formatter = new NumberFormat("#,###");

    double d1 = double.parse(numberSet.trim().replaceAll(" ", "").replaceAll(",", "").toString());
    print(d1);
    String number = formatter.format(int.parse(d1.toString().replaceAll(".0", ""))) ;
    String numShow ;
    if(number.length == 3){
      numShow = "   "+number;
    }else if(number.length == 2){
      numShow = "     "+number;
    }else if(number.length == 1){
      numShow = "     "+number;
    }else if(number.length == 4){
      numShow = ""+number;
    }else{
      numShow = number;
    }
    return numShow+" บ.";
  }
 
  getRunnumber(String number) {
    String numShow ;
    if(int.parse(number) < 10){
      numShow = " "+ number.toString()+" ";
    }else{
      numShow = number.toString();
    }
    return numShow;
  }
  getDataTime(String date) {

    var s2 = date.split(" ");
    var s1 = s2[0].split("-");

    var d1 = s1[2];
    var m1 = s1[1];
    var y1 = s1[0];
    String m2 = "";
    if(m1.toString() == "01"){m2 = "ม.ค.";}
    if(m1.toString() == "02"){m2 = "ก.พ.";}
    if(m1.toString() == "03"){m2 = "มี.ค.";}
    if(m1.toString() == "04"){m2 = "เม.ย.";}
    if(m1.toString() == "05"){m2 = "พ.ค.";}
    if(m1.toString() == "06"){m2 = "มิ.ย.";}
    if(m1.toString() == "07"){m2 = "ก.ค.";}
    if(m1.toString() == "08"){m2 = "ส.ค.";}
    if(m1.toString() == "09"){m2 = "ก.ย.";}
    if(m1.toString() == "10"){m2 = "ต.ค.";}
    if(m1.toString() == "11"){m2 = "พ.ย.";}
    if(m1.toString() == "12"){m2 = "ธ.ค.";}

    int y2 = int.tryParse(y1.toString()) + 543;
    String dataSet = d1.toString()+" "+m2+" "+y2.toString();
    return dataSet;
  }

  _onChanged1(String dataParamiter) async {
    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("dataPara", dataParamiter);
      sharedPreferences.commit();
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UserHistory()),

    );
  }
  getImageStatus(String tack) {
    String dataSetImage = "";
    if(tack == "สินค้าถึงปลายทาง" ){
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fcircle.png?alt=media&token=a0d02323-8be9-4709-84ce-60f8b1d41b58";
    }else{
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fdelivery-truck.png?alt=media&token=62f65835-907a-4db6-bf71-e772c8baf39c";
    }
    return dataSetImage;
  }


  Column buildColumn(BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              color: Theme.of(context).primaryColor,
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Card(
                  child: new ListTile(
                    //leading: new Icon(Icons.search),
                    title: new TextField(
                      controller: myController,
                      decoration: new InputDecoration(
                          hintText: "ติดตามพัสดุ", border: InputBorder.none),
                      // onChanged: onSearchTextChanged,
                    ),
                    //new Icon(Icons.search)
                    trailing: new IconButton(icon: new Image.network("https://cdn2.iconfinder.com/data/icons/mobile-and-internet-business/285/qr_code-512.png"), onPressed:_onChanged,
                    ),
                  ),
                ),
              ),
            ),

            //buildContainerB(context,myController,user),

            buildContainerImage(),
          ],
        ),

      ],
    );
  }
  void onTabTapped(int index) {
    setState(() {
      if(index ==1){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserHistoryMain()),
        );
      }else if(index == 2){
        showDialog(
            context: context,
            barrierDismissible: false,
            child: new CupertinoAlertDialog(
              content: new Text(
                "กำลังดำเนินการ",
                style: new TextStyle(fontSize: 16.0),
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    child: new Text("ตกลง"))
              ],
            ));
      }
      else if(index == 0){

      }

    });
  }

//   Future scan() async {
//     try {
//       String barcode = await BarcodeScanner.scan();
//       setState(() => this.barcode = barcode);
//       if(barcode.toUpperCase().contains("TDZ") || myController.text.toUpperCase().contains("MYN")){
//         SharedPreferences sharedPreferences;
//         sharedPreferences = await SharedPreferences.getInstance();
//         setState(() {
//           sharedPreferences.setString("checkTacking", barcode);
//           sharedPreferences.commit();
//         });
//         Navigator.push(
//             context,
//             MaterialPageRoute(builder: (context) => ReportStatus()));
//       }else if(barcode.toUpperCase().contains("?BILL=")){
//         var bill = barcode.split("?bill=");
//         _onChangedBill(bill[1]);
//       }else{
//         dic(context,"Barcode ไม่ถูกต้อง");
//       }

//     } on PlatformException catch (e) {
//       if (e.code == BarcodeScanner.CameraAccessDenied) {
//         setState(() {
//           dic(context,"The user did not grant the camera permission");
//           print("The user did not grant the camera permission!");
//           //this.barcode = '';
//         });
//       } else {

//         setState(() =>  print("Unknown error: $e"));
//       }
//     } on FormatException{
//       dic(context,"null user did not grant the camera permission");
//       setState(() => print('null (User returned using the "back"-button before scanning anything. Result)'));
//     } catch (e) {
//       dic(context,"error user did not grant the camera permission");
//       setState(() => print('Unknown error: $e'));
//     }
// }
//   getCredential() async {
//     _onChangedTacking();
//     sharedPreferences = await SharedPreferences.getInstance();
//     setState(() {
//       user = sharedPreferences.getString("checkTacking");
//     });
//   }
  _onChanged() async {



    if(myController.text == "" ){
     // scan();

    }else{
      if(myController.text.toUpperCase().contains("TDZ") || myController.text.toUpperCase().contains("MYN")){

        SharedPreferences sharedPreferences;
        sharedPreferences = await SharedPreferences.getInstance();
        setState(() {
          sharedPreferences.setString("checkTacking", myController.text);
          sharedPreferences.commit();
        });
        myController.clear();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ReportStatus()),

        );
      } else if(myController.text.toUpperCase().contains("-")){
        _onChangedBill(myController.text);
      } else{
        showDialog(
            context: context,
            barrierDismissible: false,
            child: new CupertinoAlertDialog(
              content: new Text(
                "คำเตือน \nกรุณากรอกตรวจสอบข้อมูล!!",
                style: new TextStyle(fontSize: 16.0),
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                    child: new Text("ตกลง"))
              ],
            ));

      }


    }


  }
  _onChangedBill(String dataParamiter) async {
    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("dataPara", dataParamiter);
      sharedPreferences.commit();
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UserHistory()),

    );
  }
  _onChangedTacking() async {
    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("checkTacking", "");
      sharedPreferences.commit();

    });
  }

}
dic(BuildContext context,String text){
  showDialog(
      context: context,
      barrierDismissible: false,
      child: new CupertinoAlertDialog(
        content: new Text(
          "คำเตือน \n"+text+"!!",
          style: new TextStyle(fontSize: 16.0),
        ),
        actions: <Widget>[
          new FlatButton(
              onPressed: () {
                Navigator.of(context, rootNavigator: true).pop('dialog');
                //Navigator.pop(context);
              },
              child: new Text("ตกลง"))
        ],
      ));
}

List<UserDetails> _userDetails = [];

class UserDetails {
  final String billing_no,billing_summary,billing_date,count_tracking;
  UserDetails({this.billing_no,this.billing_summary,this.billing_date,this.count_tracking});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
        count_tracking: json['count_tracking'],
      billing_no: json['billing_no'],
        billing_summary: json['billing_summary'],
      billing_date: json['billing_date']

    );
  }
}