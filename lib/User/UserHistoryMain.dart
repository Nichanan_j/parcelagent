import 'package:flutter/material.dart';
import 'package:new_s_t/MasterPage/headder.dart';
import 'package:new_s_t/System/Loading.dart';
import 'package:new_s_t/System/apiPost.dart';
import 'package:new_s_t/User/UserHistory.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';


class UserHistoryMain extends StatefulWidget {
  UserHistoryMain({Key key, this.title}) : super(key: key);
  final String title;

  //final myController ;
  @override
  // MyHomeState createState() => MyHomeState();

  MyHomeState createState() => MyHomeState();
}
class MyHomeState extends State<UserHistoryMain> {
  String jsonnn = "";
  String dataPara = "";
  String dataSetImage = "";
  String phone = "";
  var responseJson;
  String dataTitle = "";
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // Appbar
      appBar: buildAppBar(context),
      // Body
      //body:buildContainer(),//buildColumn(context),
        body: new Column(
          children: <Widget>[
            new Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Center(
                child: Card(
                  color: Colors.blueAccent.withOpacity(0.8),
                  child: Card(
                    color: Colors.white.withOpacity(1.0),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10.0,bottom: 15.0,left: 30.0,right: 30.0),
                      child: new Text(dataTitle,style: new TextStyle(fontSize: 25.0 ,color: Colors.orange.withOpacity(0.8)),),
                    ),
                  ),
                ),
              ),
            ),
            new Expanded(
                child: new ListView.builder
                  (
                    itemCount: _userDetails.length,
                    itemBuilder: (BuildContext ctxt, int Index) {
                      return new Container(
                        child: GestureDetector(
                          onTap: () {
                            _onChanged(_userDetails[Index].key);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Card(
                              color: Colors.blueAccent.withOpacity(0.8),
                              child: new Card(
                                color: Colors.white.withOpacity(1.0),

                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 15.0,right: 0.0),
                                      child: new Image.network(
                                        getImageStatus(_userDetails[Index].key),
                                        //height: double.infinity,
                                        width: 50.0,
                                      ),
                                    ),
                                    Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(top: 20.0,left: 20.0,bottom: 20.0),
                                          child: new Text(_userDetails[Index].key,
                                              style: new TextStyle(fontSize: 17.0 ,color: Colors.blueAccent.withOpacity(0.8)),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis),
                                        ),
                                        //  Padding(
                                        //    padding: const EdgeInsets.only(top: 5.0,left: 28.0,bottom: 20.0),
                                        //    child: new Text(datatest,
                                        //        style: new TextStyle(fontSize: 20.0 ,color: Colors.lightBlue.withOpacity(0.8)),
                                        //        maxLines: 2,
                                        //        overflow: TextOverflow.ellipsis),
                                        //  ),
                                      ],
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          ),

                        ),
                      );
                    }
                )
            )
          ],
        )
    );

  }


  getCredential() async {

    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      _userDetails.clear();
      phone = sharedPreferences.getString("phonenumber");
      //getUserDetails("0863769051");
      getUserDetails(phone);
    });
  }
  final String url = 'https://www.945holding.com/webservice/restful/parcel/tracking/v11/get_billing';
  Future<Null> getUserDetails(String tack) async {
    showDialog(
        context: context,
        child: progress);

    final response = await http.post(url,body: {"phoneno": tack},headers: {"apikey":apikey});
    jsonnn = response.body.replaceAll("\"", "'").replaceAll("['", "[{'key':'")
        .replaceAll("']", "'}]")
        .replaceAll(",'", "},{'key':'")
        .replaceAll("'", "\"")
    ;
    //"file_fur_point_id": "20088",
    responseJson = json.decode(jsonnn);
    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
      }
      if(_userDetails.length < 1){

        dataTitle = "ไม่มีรายการ";
      }else{
        dataTitle ="ติดตามพัสดุ";
      }

    });
    //dataTitle
    Navigator.pop(context);
  }




  void initState() {
    super.initState();
       getCredential();
      //getUserDetails("0961513730");
  }

  _onChanged(String dataParamiter) async {
    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("dataPara", dataParamiter);
      sharedPreferences.commit();
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UserHistory()),

    );
  }
  List<String> statusListData = [];
  List<String> statusListDatapoint = [];
  getDataStatus(String bill_no) {
    getStatusListHis(bill_no);
    String dataSet = "";
    for(int i= 0; i < _userDetailsHis.length;i++){
      getStatusListPoint(_userDetailsHis[i].key);
      String  data = "";
      try{
        data = _StatusList[_StatusList.length - 1].description;
        statusListData.add(data);
      }catch(e){
      }
    }
    for(int n = 0; n < statusListData.length;n++){
      if(statusListData[n] == "สินค้าถึงปลายทาง"){
        statusListDatapoint.add("สินค้าถึงปลายทาง");
      }
    }
    if(statusListDatapoint.length > 0 ){
      dataSet = "สินค้าถึงปลายทาง";
      //dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fcircle.png?alt=media&token=a0d02323-8be9-4709-84ce-60f8b1d41b58";

    }else if(statusListDatapoint.length == 0){
      //dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fdelivery-truck.png?alt=media&token=62f65835-907a-4db6-bf71-e772c8baf39c";
      dataSet =  "สินค้าอยู่ระหว่างการจัดส่ง";
    }
    return dataSet;
  }
  getImageStatus(String tack) {
    String dataSetImage = "";
    if(tack == "สินค้าถึงปลายทาง" ){
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fcircle.png?alt=media&token=a0d02323-8be9-4709-84ce-60f8b1d41b58";
    }else{
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fdelivery-truck.png?alt=media&token=62f65835-907a-4db6-bf71-e772c8baf39c";
    }
    return dataSetImage;
  }

  Future<Null> getStatusListPoint(String tack) async {
    final response = await http.post(from_trackingURL,body: {"con_no": tack},headers: {"apikey":apikey});
    final responseJson = json.decode(response.body);
    setState(() {
      for (Map user in responseJson) {
        _StatusList.add(StatusList.fromJson(user));
      }
    });

  }

Future<Null> getStatusListHis(String tack) async {
    final response = await http.post(BuildingURL,body: {"bill_no": tack},headers: {"apikey":apikey});
    final responseJson = json.decode(
        response.body.replaceAll("\"", "'").replaceAll("['", "[{'key':'")
        .replaceAll("']", "'}]")
        .replaceAll(",'", ",'key':'")
        .replaceAll("'", "\""));
    setState(() {
      for (Map user in responseJson) {
        _userDetailsHis.add(UserDetailsHis.fromJson(user));
      }
    });

  }

}

List<UserDetails> _userDetails = [];
class UserDetails {
  final String key;
  UserDetails({this.key});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
      key: json['key'],

    );
  }
}
List<UserDetailsHis> _userDetailsHis = [];
class UserDetailsHis {
  final String key;
  UserDetailsHis({this.key});

  factory UserDetailsHis.fromJson(Map<String, dynamic> json) {
    return new UserDetailsHis(
      key: json['key'],

    );
  }
}


List<StatusList> _StatusList = [];
class StatusList {
  final String id;
  final String description, timestamp,city,countryCode,from,circleimage,deliverytruckimage;
  StatusList({this.id, this.description, this.timestamp, this.city,this.countryCode,this.from,this.circleimage =  "circle.png",this.deliverytruckimage = "deliverytruck.png"});

  factory StatusList.fromJson(Map<String, dynamic> json) {
    return new StatusList(
      id: json['shipmentID'],
      description: json['description'],
      timestamp: json['timestamp'],
      city: json['city'],
      countryCode: json['countryCode'],
      from:json["from"],

    );
  }
}

