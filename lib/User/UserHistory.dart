import 'package:flutter/material.dart';
import 'package:new_s_t/MasterPage/headder.dart';
import 'package:new_s_t/Report/RePortStatus.dart';
import 'package:new_s_t/System/Loading.dart';
import 'package:new_s_t/System/apiPost.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';


class UserHistory extends StatefulWidget {
  UserHistory({Key key, this.title}) : super(key: key);
  final String title;


  //final myController ;
  @override
  // MyHomeState createState() => MyHomeState();

  MyHomeState createState() => MyHomeState();
}
class MyHomeState extends State<UserHistory> {
  String jsonnn = "",datatest="";
  String building = "";
  String fateRespo ="";
  SharedPreferences sharedPreferences;
  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      building = sharedPreferences.getString("dataPara");
      getUserDetails(building);
    });
  }
  void initState() {
    super.initState();
    _userDetails.clear();
    getCredential();
    //  getUserDetails(building);
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // Appbar
      appBar: buildAppBarHis(context,building),
      // Body
        body:  buildColumn(),
       // body :buildColumn(context),
    );
  }

  Column buildColumn() {
    return new Column(
        children: <Widget>[

          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:15.0,left: 15.0,right: 0.0),
                child: Column(
                  children: <Widget>[
                    new Text(building,style:
                    new TextStyle(fontSize: 24.0 ,color: Colors.black.withOpacity(0.8),
                        fontStyle: FontStyle.normal),textAlign: TextAlign.left,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis),
                  ],
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:0.0,left: 15.0,right: 0.0),
                child: Column(
                  children: <Widget>[
                    new Text(fateRespo ,style:
                    new TextStyle(fontSize: 18.0 ,color: Colors.grey.withOpacity(1.0),
                        fontStyle: FontStyle.normal),textAlign: TextAlign.left,),
                  ],
                ),
              ),
            ],
          ),


          new Expanded(
              child: new ListView.builder
                (
                  itemCount: _userDetails.length,
                  itemBuilder: (BuildContext ctxt, int Index) {
                    return new Container(
                      child: GestureDetector(
                        onTap: () {
                          _onChanged(_userDetails[Index].tracking);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left:10.0,right: 10.0),
                          child: new Container(
                            color: Colors.blueAccent.withOpacity(0.8),
                            padding: const EdgeInsets.only(bottom: 2.0),
                            child: new Container(
                              color: Colors.white.withOpacity(1.0),

                              child: Column(
                                children: <Widget>[

                                  Row(
                                      children: <Widget>[
                                        new Expanded(
                                          child: new Container(
                                            padding: const EdgeInsets.only(top: 10.0,left: 10.0,right: 0.0),
                                            child: new Center(
                                              child: new InkWell(// this is the one you are looking for..........

                                                child: new Container(
                                                  //width: 50.0,
                                                  //height: 50.0,
                                                  padding: const EdgeInsets.all(10.0),//I used some padding without fixed width and height
                                                  decoration: new BoxDecoration(
                                                    shape: BoxShape.circle,// You can use like this way or like the below line
                                                    //borderRadius: new BorderRadius.circular(30.0),
                                                    color: getColorStatus(_userDetails[Index].order_status_TH),
                                                  ),
                                                  child: new Text("".toString(), style: new TextStyle(color: Colors.white, fontSize: 0.0)),// You can add a Icon instead of text also, like below.
                                                  //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                                ),//............
                                              ),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Expanded(
                                          child: new Container(
                                            padding: const EdgeInsets.only(top: 13.0,left: 0.0,bottom: 0.0),
                                            child: new Text(_userDetails[Index].order_phoneno,
                                                style: new TextStyle(fontSize: 16.0 ,color: Colors.blueAccent.withOpacity(0.8)),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          flex: 2,
                                        ),
                                        new Expanded(
                                          child: new Container(
                                          ),
                                          flex: 1,
                                        ),
                                        new Expanded(
                                          child: new Container(
                                            padding: const EdgeInsets.only(top: 10.0,left: 0.0,bottom: 0.0),
                                            child:  new Text(_userDetails[Index].order_name,textAlign: TextAlign.left,
                                                style: new TextStyle(fontSize: 16.0 ,color: Colors.blueAccent.withOpacity(0.8)),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,),
                                          ),
                                          flex: 2,
                                        ),


                                      ]
                                  ),
                                  Row(
                                      children: <Widget>[
                                        new Expanded(
                                          child: new Container(

                                          ),
                                          flex: 2,
                                        ),
                                        new Expanded(
                                          child: new Container(
                                            padding: const EdgeInsets.only(top: 10.0,left: 0.0,bottom: 10.0,right: 10.0),
                                            child:  new Text(getStatusString(_userDetails[Index].order_status_TH),
                                                style: new TextStyle(fontSize: 14.0 ,color: Colors.black.withOpacity(0.8)),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          flex: 4,
                                        ),
                                        //,background: getColorCod(_userDetails[Index].method_name_EN)
                                        new Expanded(
                                          child: new Container(
                                            child: new InkWell(

                                              child: new Container(
                                                margin: EdgeInsets.only(left: 20.0,right: 2.0,top: 5.0,bottom: 5.0),
                                                padding: EdgeInsets.only(right: 0.0),

                                                height: 20.0,
                                                decoration: new BoxDecoration(
                                                  color: getColorCod(_userDetails[Index].method_name_EN),

                                                  borderRadius: new BorderRadius.circular(50.0),
                                                ),
                                                child: new Center(child: new Text(_userDetails[Index].method_name_EN.replaceAll("Cash", "NOR")
                                                     ,textAlign: TextAlign.right, style: new TextStyle(color: Colors.white
                                                       , fontSize: 7.0
                                                   ),
                                                   ),),
                                              ),
                                            ),

                                          ),
                                          flex: 2,
                                        ),
                                        new Expanded(
                                          child: new Container(

                                            child: new Text(_userDetails[Index].tracking,textAlign: TextAlign.left,
                                                style: new TextStyle(fontSize: 14.0 ,color: Colors.black54.withOpacity(1.0)),
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis),
                                          ),
                                          flex: 4,
                                        ),
                                       // Column(
                                       //   children: <Widget>[
                                       //     Padding(
                                       //       padding: const EdgeInsets.only(top: 10.0,left: 45.0,bottom: 10.0,right: 10.0),
                                       //       child: new Text(_userDetails[Index].order_status_TH,
                                       //           style: new TextStyle(fontSize: 14.0 ,color: Colors.black.withOpacity(0.8)),
                                       //           maxLines: 2,
                                       //           overflow: TextOverflow.ellipsis),
                                       //     ),
                                       //   ],
                                       // ),
                                        //new Container(
                                        //  padding: const EdgeInsets.only(left: 25.0,right: 0.0),
                                        //  child: new InkWell(// this is the one you are looking for..........
//
                                        //    child: new Container(
                                        //      //width: 50.0,
                                        //      //height: 50.0,
                                        //      padding: const EdgeInsets.only(top:5.0,left: 5.0,right: 5.0,bottom: 5.0),//I used some padding without fixed width and height
                                        //      decoration: new BoxDecoration(
                                        //        shape: BoxShape.circle,// You can use like this way or like the below line
                                        //        //borderRadius: new BorderRadius.circular(30.0),
                                        //        color: Colors.green,
                                        //      ),
                                        //      child: new Text(_userDetails[Index].method_name_EN, style: new TextStyle(color: Colors.white, fontSize: 10.0)),// You can add a Icon instead of text also, like below.
                                        //      //child: new Icon(Icons.arrow_forward, size: 50.0, color: Colors.black38)),
                                        //    ),//............
                                        //  ),
                                        //),
                                       // Column(
                                       //   mainAxisAlignment: MainAxisAlignment.center,
                                       //   children: <Widget>[
                                       //     Padding(
                                       //       padding: const EdgeInsets.only(top: 15.0,left: 2.0,bottom: 10.0),
                                       //       child: new Text(_userDetails[Index].tracking,textAlign: TextAlign.right,
                                       //           style: new TextStyle(fontSize: 14.5 ,color: Colors.black54.withOpacity(1.0)),
                                       //           maxLines: 2,
                                       //           overflow: TextOverflow.ellipsis),
                                       //     ),
                                       //   ],
                                       // ),

                                      ]
                                  )
                                ],
                              ),

                            ),
                          ),
                        ),

                      ),
                    );
                  }
              )
          )
        ],
      );
  }

  final String url = 'https://www.945holding.com/webservice/restful/parcel/tracking/v11/from_billing';
  Future<Null> getUserDetails(String tack) async {
    showDialog(
        context: context,
        child: progress);
    final response = await http.post(url,body: {"bill_no": tack},headers: {"apikey":apikey});
    jsonnn = response.body;
    //"file_fur_point_id": "20088",
    print("responseJson1::"+jsonnn);
    try {
      final responseJson = json.decode(jsonnn);
      setState(() {
        for (Map user in responseJson) {
          _userDetails.add(UserDetails.fromJson(user));
        }
        fateRespo = getDataTime(_userDetails[0].order_date);
      });
      Navigator.of(context, rootNavigator: true).pop('dialog');
    }catch(e){
      Navigator.of(context, rootNavigator: true).pop('dialog');
    }
    //Navigator.pop(context);
  }
  final String statusUrl = 'https://www.945holding.com/webservice/restful/parcel/tracking/v11/from_billing';
  Future<Null> getStatusList(String tack) async {
    final response = await http.post(url,body: {"bill_no": tack},headers: {"apikey":apikey});
    jsonnn = response.body;
    //"file_fur_point_id": "20088",
    final responseJson = json.decode(jsonnn);
    setState(() {
      for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
      }
      fateRespo = getDataTime(_userDetails[0].order_date);
    });
  }



  _onChanged(String takking) async {
    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("checkTacking", takking);
      sharedPreferences.commit();
    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ReportStatus()),

    );
  }
  getColorStatus(String status) {
    Color color;
    if(status == "กำลังจัดส่ง"){
       color = Colors.orange;
    }else if(status == "สินค้าถึงปลายทาง"){
       color =Colors.green;
    }else if(status == "สินค้าตีกลับ"){
       color =Colors.red;
    }else if(status == "ยกเลิกรายการ"){
      color =Colors.red;
    }else{
      color = Colors.orange;
    }
  return color;
  }
  getColorCod(String status) {
    Color color;
    if(status == "Cash"){
      color = Colors.blueAccent;
    }else if(status == "Cod"){
      color =Colors.green;
    }else{
      color = Colors.green;
    }
    return color;
  }
  getStatusString(String status) {
    String statu;
    if(status == "กำลังจัดส่ง"){
      statu = "อยู่ระหว่างการจัดส่ง";
    }else if(status == "สินค้าถึงปลายทาง"){
      statu = "จัดส่งสำเร็จ";
    }else if(status == "สินค้าตีกลับ"){
      statu = "ยกเลิก";
    }else if(status == "ยกเลิกรายการ"){
      statu = "ยกเลิก";
    }else{
      statu = "อยู่ระหว่างการจัดส่ง";
    }
    return statu;
  }
  getDataTime(String date) {

    var s2 = date.split(" ");
    var s1 = s2[0].split("-");

    var d1 = s1[2];
    var m1 = s1[1];
    var y1 = s1[0];
    String m2 = "";
    if(m1.toString() == "01"){m2 = "ม.ค.";}
    if(m1.toString() == "02"){m2 = "ก.พ.";}
    if(m1.toString() == "03"){m2 = "มี.ค.";}
    if(m1.toString() == "04"){m2 = "เม.ย.";}
    if(m1.toString() == "05"){m2 = "พ.ค.";}
    if(m1.toString() == "06"){m2 = "มิ.ย.";}
    if(m1.toString() == "07"){m2 = "ก.ค.";}
    if(m1.toString() == "08"){m2 = "ส.ค.";}
    if(m1.toString() == "09"){m2 = "ก.ย.";}
    if(m1.toString() == "10"){m2 = "ต.ค.";}
    if(m1.toString() == "11"){m2 = "พ.ย.";}
    if(m1.toString() == "12"){m2 = "ธ.ค.";}

    int y2 = int.tryParse(y1.toString()) + 543;
    String dataSet = d1.toString()+" "+m2+" "+y2.toString();
    return dataSet;
  }
  getDataStatus(String tack) {
    String dataSet = "";

    getStatusListPoint(tack);
    try{
        dataSet = _StatusList[0].description.substring(0,23);
        //_StatusList.clear();

        //dataSet +=_StatusList[0].description.substring(23,dataSet.length);
    }catch(e){
    }
    print("test::"+dataSet);

    return dataSet;
  }
  getImageStatus(String tack) {
    String dataSetImage = "";
    if(tack == "สินค้าถึงปลายทาง" ){
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fcircle.png?alt=media&token=a0d02323-8be9-4709-84ce-60f8b1d41b58";
    }else{
      dataSetImage = "https://firebasestorage.googleapis.com/v0/b/my945app.appspot.com/o/Tracking%20App%2Fdelivery-truck.png?alt=media&token=62f65835-907a-4db6-bf71-e772c8baf39c";
    }
    return dataSetImage;
  }

    Map<String,dynamic> map = new Map();
    Future<Null> getStatusListPoint(String tack) async {
      _StatusList.clear();
      final response = await http.post(from_trackingURL,body: {"con_no": tack},headers: {"apikey":apikey});
      final responseJson = json.decode(response.body);
      print("responseJson1::"+response.body);

      setState(() {
        for (Map user in responseJson) {
          _StatusList.add(StatusList.fromJson(user));
        }

       // map.addAll(_StatusList[0].description,'');

      });

    }
}
List<UserDetails> _userDetails = [];
class UserDetails {
  final String tracking;
  final String parcel_member_id;
  final String parcel_member_name;
  final String order_name;
  final String order_phoneno;
  final String order_status_TH;
  final String order_status_EN;
  final String method_name_TH;
  final String method_name_EN;
  final String order_date;

  UserDetails({this.tracking,this.parcel_member_id,this.parcel_member_name,this.order_name,this.order_phoneno,this.order_status_TH,this.order_status_EN
    ,this.method_name_TH,this.method_name_EN,this.order_date});
  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
      tracking: json['tracking'],
      parcel_member_id: json['parcel_member_id'],
      parcel_member_name: json['parcel_member_name'],
      order_name: json['order_name'],
      order_phoneno: json['order_phoneno'],
      order_status_TH: json['order_status_TH'],
      order_status_EN: json['order_status_EN'],
      method_name_TH: json['method_name_TH'],
      method_name_EN: json['method_name_EN'],
      order_date: json['order_date'],

    );
  }
}

List<StatusList> _StatusList = [];
List<String> _StatusListData = [];

class StatusList {
  final String id;
  final String description, timestamp,city,countryCode,from,circleimage,deliverytruckimage;
  StatusList({this.id, this.description, this.timestamp, this.city,this.countryCode,this.from,this.circleimage =  "circle.png",this.deliverytruckimage = "deliverytruck.png"});

  factory StatusList.fromJson(Map<String, dynamic> json) {
    return new StatusList(
      id: json['shipmentID'],
      description: json['description'],
      timestamp: json['timestamp'],
      city: json['city'],
      countryCode: json['countryCode'],
      from:json["from"],

    );
  }
}




