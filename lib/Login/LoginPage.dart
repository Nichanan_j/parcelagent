import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:new_s_t/Home/webview.dart';
import 'package:new_s_t/System/Loading.dart';
import 'package:new_s_t/System/apiPost.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:async';
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController phonNumber = new TextEditingController();
  TextEditingController OTP = new TextEditingController();
  String ref = "";
  String refCode = "";
  bool checkValue = true;
  String jsonn  ="";
  SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();

    getCredential();
  }
  final String url = 'https://www.945holding.com/webservice/restful/otp/v10/sendotp/';
  Future<Null> getUserDetails(String phone) async {
    _userDetails.clear();
    _userDetailsOTP.clear();
    final response = await http.post(url,body: {"apikey": apikey,"merid":"1","phoneno":phone});

    final responseJson = json.decode(response.body.replaceAll("{","[{").replaceAll("}","}]"));

    setState(() {
        for (Map user in responseJson) {
        _userDetails.add(UserDetails.fromJson(user));
      }
    });

    jsonn  =_userDetails[0].status;
    Navigator.of(context, rootNavigator: true).pop('dialog');

    showDialog(
      context: context,
      builder: (context) {
        return Center(
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: new Row(
              children: <Widget>[
                new Expanded(
                  child: new TextField(
                    controller: OTP,
                    autofocus: false,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)),
                      //hintText: 'Enter your phone number',
                      labelText: 'OTP',
                      labelStyle: new TextStyle(fontSize: 20.0),
                      prefixIcon: const Icon(
                        Icons.phone_android,
                        color: Colors.blueAccent,
                      ),
                    ),

                  ),
                )
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('ยกเลิก'),

                  textColor: Colors.white,
                  color: Colors.orange,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                  }),
              new FlatButton(

                  child: const Text('ตกลง'),
                  textColor: Colors.white,
                  color: Colors.green,
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                    if(_userDetails[0].refcode != "" ){
                      ref = _userDetails[0].recordid;
                      refCode = _userDetails[0].refcode;
                    }

                    showDialog(
                        context: context,
                        child: progress);

                    getUserDetailsOTP(phonNumber.text,refCode,ref,OTP.text);
                  })
            ],

          ),
        );
      },
    );
  }
  final String urlOTP = 'https://www.945holding.com/webservice/restful/otp/v10/checkotp/';

  Future<Null> getUserDetailsOTP(String phone,String refcode,String recordid,String otpcode) async {
    jsonn +="OTP ::"+ phone+refcode+recordid+otpcode;
    final response1 = await http.post(urlOTP,body: {"apikey": apikey,"merid":"1","phoneno":phone,"refcode":refcode,"recordid":recordid,"otpcode": otpcode});

    final responseJson1 = json.decode(response1.body.replaceAll("{","[{").replaceAll("}","}]"));

    setState(() {
      for (Map user1 in responseJson1) {
        _userDetailsOTP.add(UserDetailsOTP.fromJsonOtp(user1));
      }
    });
    //jsonn +="OTP ::"+ response1.body;
    if(_userDetailsOTP[0].status == "SUCCESS"){
      Navigator.of(context, rootNavigator: true).pop('dialog');
      _onChanged();
      Navigator.of(context).pushAndRemoveUntil(
          new MaterialPageRoute(
              builder: (BuildContext context) => new MyAppHomePage()),
              (Route<dynamic> route) => false);
    }else{

      Navigator.of(context, rootNavigator: true).pop('dialog');
     // OTP.clear();
      //OTP.text = jsonn;
      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "OTP ไม่ถูกต้อง!!",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                  },
                  child: new Text("OK"))
            ],
          ));
    }

  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Search Tracking"),
        elevation: 0.0,
        backgroundColor: Colors.blueAccent,
      ),
      body: new SingleChildScrollView(
        child: _body(),
        scrollDirection: Axis.vertical,
      ),
    );
  }

  Widget _body() {
    return new Container(
      color: Colors.white.withOpacity(1.0),
      padding: EdgeInsets.only(right: 20.0, left: 20.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          new Container(
            margin: EdgeInsets.all(30.0),
            child: new Image.asset(
              "assets/logostart.png",
              height: 250.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20.0,left: 20.0,top: 50.0,bottom: 20.0),
            child: new TextField(
              controller: phonNumber,
              keyboardType: TextInputType.number,
              maxLength: 10,
              decoration: InputDecoration(
                  border: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.white)),
                  //hintText: 'Enter your phone number',
                  labelText: 'Enter your phone number',
                  labelStyle: new TextStyle(fontSize: 18.0),
                  prefixIcon: const Icon(
                  Icons.phone_android,
                  color: Colors.blueAccent,
                  ),
                  ),


            ),


          ),


          Padding(
            padding: const EdgeInsets.only(left: 35.0,right: 35.0,top: 15.0,bottom: 50.0),
            child: new Container(
              decoration:

                  new BoxDecoration(
                      color: Colors.blueAccent,
                      border: new Border.all(
                      color: Colors.white,
                      width: 4.0
                  ),
                      borderRadius: new BorderRadius.circular(20.0)
                  ),
              child: new ListTile(
                title: new Text(
                  "Login",
                  style: new TextStyle(fontSize: 25.0 ,color: Colors.white.withOpacity(1.0)),
                  textAlign: TextAlign.center,
                ),
                onTap: _navigator,
              ),
            ),
          )
        ],
      ),
    );
  }

  _onChanged() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      sharedPreferences.setString("phonenumber", phonNumber.text);
      sharedPreferences.commit();

    });
  }

  getCredential() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      phonNumber.text = sharedPreferences.getString("phonenumber");
      if(sharedPreferences.getString("phonenumber") != "" && sharedPreferences.getString("phonenumber") != null ){
        Navigator.of(context).pushAndRemoveUntil(
            new MaterialPageRoute(
                builder: (BuildContext context) => new MyAppHomePage()),
                (Route<dynamic> route) => false);
      }


     // checkValue = sharedPreferences.getBool("check");
     // if (checkValue != null) {
     //   if (checkValue) {
     //     phonNumber.text = sharedPreferences.getString("phonenumber");
     //   } else {
     //     phonNumber.clear();
     //     sharedPreferences.clear();
     //   }
     // } else {
     //   checkValue = false;
     // }
    });
  }

  _navigator() {

    if (phonNumber.text.length == 10) {
      showDialog(
          context: context,
          child: progress);
      getUserDetails(phonNumber.text);

      //_onChanged();
      //Navigator.of(context).pushAndRemoveUntil(
      //    new MaterialPageRoute(
      //        builder: (BuildContext context) => new MyAppHomePage()),
      //        (Route<dynamic> route) => false);
    } else {

      showDialog(
          context: context,
          barrierDismissible: false,
          child: new CupertinoAlertDialog(
            content: new Text(
              "กรุณาตรวจสอบเบอร์โทรอีกครั้ง!!",
              style: new TextStyle(fontSize: 16.0),
            ),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                  },
                  child: new Text("OK"))
            ],
          ));
    }
  }

}

List<UserDetails> _userDetails = [];
class UserDetails {
  final String status,phone_no,refcode,recordid;
  UserDetails({this.status,this.phone_no,this.refcode,this.recordid});

  factory UserDetails.fromJson(Map<String, dynamic> json) {
    return new UserDetails(
      status: json['status'],
      phone_no: json['phone_no'],
      refcode: json['refcode'],
      recordid: json['recordid'],

    );
  }
}

List<UserDetailsOTP> _userDetailsOTP = [];
class UserDetailsOTP {
  final String status;
  UserDetailsOTP({this.status});

  factory UserDetailsOTP.fromJsonOtp(Map<String, dynamic> json) {
    return new UserDetailsOTP(
      status: json['status'],
    );
  }
}
