import 'package:flutter/material.dart';
import 'package:new_s_t/Home/index.dart';
import 'package:new_s_t/Home/webview.dart';
import 'package:new_s_t/Login/LoginPage.dart';
import 'package:new_s_t/Opencod/Opencod.dart';
import 'package:new_s_t/Opencod/autocomplete.dart';
import 'package:new_s_t/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

MaterialApp buildMaterialApp() {
  return new MaterialApp(
    title: 'MY945.ME',
    debugShowCheckedModeBanner: false,
    theme: new ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: new LoginPage(),
  );
}

AppBar buildAppBar(BuildContext context) {
  return new AppBar(
    title: new Text(
      "MY945.ME",
      textAlign: TextAlign.center,
    ),
    actions: <Widget>[
      new IconButton(
          icon: new Icon(Icons.exit_to_app),
          onPressed: () async {
            SharedPreferences sharedPreferences;
            sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.clear();
            Navigator.of(context).pushAndRemoveUntil(
                new MaterialPageRoute(
                    builder: (BuildContext context) => new LoginPage()),
                (Route<dynamic> route) => false);
          }),
      new IconButton(
          icon: new Icon(Icons.library_books),
          onPressed: () async {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (BuildContext context) => new Confirm()));
          })
    ],
  );
// Navigator.push(
//                       context,
//                       MaterialPageRoute(builder: (context) => MyAppHomePageReport()),
}

AppBar buildAppBarNotLogout(BuildContext context) {
  return new AppBar(
    title: new Text(
      "MY945.ME",
      textAlign: TextAlign.center,
    ),
    actions: <Widget>[],
  );
}

AppBar buildDashboard(BuildContext context) {
  return new AppBar(
    title: new Text(
      "Dashboard",
      textAlign: TextAlign.center,
    ),
    actions: <Widget>[],
  );
}

AppBar buildAppBarWebview(BuildContext context) {
  return new AppBar(
    //title: new Text("MY945.ME",textAlign: TextAlign.center,),
    actions: <Widget>[],
  );
}

AppBar buildAppBarHis(BuildContext context, String title) {
  return new AppBar(
    title: new Text(
      "ติดตามบิลใบเสร็จ" + "\nเลขที่บิล : " + title,
      textAlign: TextAlign.left,
      style: new TextStyle(fontSize: 14.0),
    ),
    actions: <Widget>[],
  );
}

BottomNavigationBar buildBottomNavigationBar(
    void Function(int index) onTabTapped, int _currentIndex) {
  return BottomNavigationBar(
    iconSize: 30.0,
    currentIndex: _currentIndex,
    onTap: onTabTapped, // this will be set when a new tab is tapped
    items: [
      BottomNavigationBarItem(
        icon: new Icon(Icons.home),
        title: new Text('หน้าหลัก'),
      ),
      BottomNavigationBarItem(
        icon: new Icon(Icons.dashboard),
        title: new Text('ติดตามพัสดุ'),
      ),
      //  BottomNavigationBarItem(
      //      icon: Icon(Icons.receipt),
      //      title: Text('รายงาน')
//
      //  )
    ],
    //type: BottomNavigationBarType.fixed,
  );
}

void navigationTapped(int page) {}
