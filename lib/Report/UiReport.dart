import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:async';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:new_s_t/System/apiPost.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
final List<String> imgList = ["https://www.945holding.com/banner_app/945.jpg","https://www.945holding.com/banner_app/945.jpg"];

String urlSet = "https://www.945holding.com/webservice/restful/parcel/app_dashboard/v11/get_banner";
String urlSetBox = "https://www.945holding.com/webservice/restful/parcel/app_dashboard/v11/get_sum_box";
List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i  ++) {
    result.add(handler(i, list[i]));
  }
  return result;
}
List<StatusList> _StatusListData = [];
PageController controller;
int currentpage = 0;
class StatusList {
  final String image;
  final String link;
  StatusList({this.image, this.link});

  factory StatusList.fromJson(Map<String, dynamic> json) {
    return new StatusList(
      image: json['image'],
      link: json['link'],
    );
  }
}

List<StatusListBoxO> _StatusListDataBox = [];

class StatusListBoxO {
  final String mini;
  final String minip;
  final String l;
  final String mp;
  final String m;
  final String sp;
  final String xl;
  final String xxl;
  final String s;
  StatusListBoxO({this.mini = "0", this.s = "0", this.l= "0", this.xl= "0", this.xxl= "0", this.m= "0", this.mp= "0", this.sp= "0"
    , this.minip= "0"});

  factory StatusListBoxO.fromJson(Map<String, dynamic> json) {
    return new StatusListBoxO(
      mini: json['mini'],
      s: json['s'],
      l: json['l'],

      m: json['m'],
      mp: json['m-plus'],
      sp: json['s-plus'],
      minip: json['mini-plus'],
      xl: json['xl'],
      xxl: json['xxl'],
    );
  }
}
class MyAppHomePageReport extends StatefulWidget {
  MyAppHomePageReport({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MyHomeState createState() => MyHomeState();

}

class MyHomeState extends State<MyAppHomePageReport> {
  CarouselSlider instance;
  String phone = "";
  void initState() {
    controller = new PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 0.5,
    );
    super.initState();
    getCredential1();
    getStatusListPoint();

  }
  Future<Null> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }
  @override
  Widget build(BuildContext context) {

    // print(instance.nextPage());
    return new MaterialApp(
      title: 'Dashboard',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),

      home: new Scaffold(
          appBar: new AppBar(title: new Text('Dashboard')),
        body: buildContainer(),
      ),

    );

  }
  Center buildCenter() {
    return new Center(
      child: new Container(
        child: new PageView.builder(
            onPageChanged: (value) {
              setState(() {
                currentpage = value;
              });
            },
            controller: controller,
            itemBuilder: (context, index) => builder(index)),
      ),
    );
  }
  void _showToast(BuildContext context) {
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: const Text('Added to favorite'),
        action: SnackBarAction(
            label: 'UNDO', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
  Container buildContainer() {
    return Container(

      // width: 350.0,
        child: Container(
           padding: EdgeInsets.only(bottom: 30.0),
            margin: EdgeInsets.symmetric(vertical: 10.0),
            //width: 200.0,

          child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

          buildExpanded(),

        new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: new Row(
                children: <Widget>[
                  buildPaddingData("mini"),
                  buildPaddingData("mini-plus"),
                  buildPaddingData("s"),
                  buildPaddingData("s-plus"),
                  buildPaddingData("m"),
                  buildPaddingData("m-plus"),
                  buildPaddingData("l"),
                  buildPaddingData("xl"),
                  buildPaddingData("xxl"),
                  // ...
                ],
              ),
            ),
          ],
        ),
      ]
    )
    )
  );
  }
  getCredential1() async {

    SharedPreferences sharedPreferences;
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      phone = sharedPreferences.getString("phonenumber");
      //getUserDetails("0863769051");
      // getUserDetails("0951825674");
      //print("12121212"+phone);
      getStatusListPointBox(phone);
    });
  }

  Expanded buildExpanded() {
    return new Expanded(
        child: PageView.builder(

        scrollDirection: Axis.horizontal,
            itemCount: _StatusListData.length,
            itemBuilder: (BuildContext ctxt, int Index) {
              return new Center(
                  child: GestureDetector(
                      onTap: () {
                        print(_StatusListData[Index].link);
                        if(_StatusListData[Index].link != ""){
                          _launchInBrowser(_StatusListData[Index].link);
                        }else{
                          _showToast;
                          Fluttertoast.showToast(
                              msg: "รูปนี้ไมมี Link Url",
                              toastLength: Toast.LENGTH_SHORT
                             );
                        }

                      },
                      child: Container(

                        margin: new EdgeInsets.all(5.0),
                        child: new Image.network(_StatusListData[Index].image,
                          fit:BoxFit.contain,
                          height: double.infinity,
                        ),
                      )
                  )
              );
            }
              )
        );
  }
  builder(int index) {
    return new AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page - index;
          value = (1 - (value.abs() * .8)).clamp(0.0, 1.0);
        }

        return new Center(
          child: new SizedBox(
            height: Curves.easeOut.transform(value) * 300,
            width: Curves.easeOut.transform(value) * 250,
            child: child,
          ),
        );
      },
        child: Container(
          margin: new EdgeInsets.all(0.0),
          child: new Image.network(_StatusListData[index].image,
            fit:BoxFit.contain,
            height: double.infinity,
          ),
        )
    );
  }
  Padding buildPaddingData(String type) {
    String typ = 'assets/'+type+'.png';
    if(_boxText(type) != "0"){
      return buildPadding(typ, type);
    }else{
      return Padding(
        padding: const EdgeInsets.all(0.0),
        child: new Text(""),
      );
    }

  }

  Padding buildPadding(String typ, String type) {
    return Padding(
               padding: const EdgeInsets.only(bottom:  10.0,left: 10.0,right: 10.0),
               child: Column(
                 children: <Widget>[
                   Material(
                     elevation: 0.0,
                     shape: CircleBorder(),
                      color: Colors.blue,
                     child: Column(
                       children: <Widget>[
                         Padding(
                           padding: const EdgeInsets.only(top: 30.0),
                           child: Ink.image(
                             image: AssetImage(typ),
                             fit: BoxFit.cover,
                             width: 70.0,
                             height: 70.0,
                             child: InkWell(

                             ),
                           ),
                         ),
                         Material(
                           elevation: 0.0,
                           shape: CircleBorder(),
                           color: Colors.blue,
                           child: Column(
                             children: <Widget>[
                               Ink.image(
                                 image: AssetImage(""),
                                 fit: BoxFit.cover,

                                 height: 40.0,
                                 child: InkWell(

                                   child: Padding(
                                     padding: const EdgeInsets.all(13.0),
                                     child: Text(_boxText(type),
                                       style: new TextStyle(color: Colors.white,fontSize: 14.0)

                                       ,),
                                   )
                                 ),
                               ),


                             ],
                           ),

                         ),

                       ],
                     ),

                   ),
                 //  Material(
                 //    elevation: 0.0,
                 //    shape: CircleBorder(),
                 //    color: Colors.blue,
                 //    child: Column(
                 //      children: <Widget>[
//
                 //        Text(_boxText(type),style: new TextStyle(color: Colors.black,fontSize: 18.0),)
//
                 //      ],
                 //    ),
//
                 //  ),
                 ],
               ),
             );
  }
  _boxText(String type){
    String text = "0";
    try{print(_StatusListDataBox[0].mini.toString());

     if(_StatusListDataBox[0].mini != "" && type == "mini"&& _StatusListDataBox[0].mini != null){
       text = _StatusListDataBox[0].mini ;
     }else if(_StatusListDataBox[0].xxl != "" && type == "xxl"&& _StatusListDataBox[0].xxl != null){
       text = _StatusListDataBox[0].xxl ;
     }else if(_StatusListDataBox[0].xl != "" && type == "xl"&& _StatusListDataBox[0].xl != null){
       text = _StatusListDataBox[0].xl ;
     }else if(_StatusListDataBox[0].mp != "" && type == "m-plus"&& _StatusListDataBox[0].mp != null){
       text = _StatusListDataBox[0].mp ;
     }else if(_StatusListDataBox[0].minip != "" && type == "mini-plus" && _StatusListDataBox[0].minip != null){
       text = _StatusListDataBox[0].minip ;
     }else if(_StatusListDataBox[0].s != "" && type == "s" && _StatusListDataBox[0].s != null){
       text = _StatusListDataBox[0].s ;
     }else if(_StatusListDataBox[0].sp != "" && type == "s-plus" && _StatusListDataBox[0].sp != null ){
       text = _StatusListDataBox[0].sp ;
     }else if(_StatusListDataBox[0].l != "" && type == "l" && _StatusListDataBox[0].l != null){
       text = _StatusListDataBox[0].l ;
     }else if(_StatusListDataBox[0].m != "" && type == "m" && _StatusListDataBox[0].m != null){
      text = _StatusListDataBox[0].m ;
      }
    }catch(e){}
    final formatter = new NumberFormat("#,###");
    return formatter.format(int.parse(text)) ;
  }

  ListView buildListView() {
    return new ListView(
            children: <Widget>[
              new Container(
                  padding: new EdgeInsets.symmetric(horizontal: 15.0),
                  height: 1000.0,
                  child: new CarouselSlider(
                    items: map<Widget>(imgList, (index, i) {
                      return new Container(

                          margin: new EdgeInsets.all(5.0),
                          child: new ClipRRect(
                              borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                              child: new Stack(
                                children: <Widget>[
                                  new Image.network(i,
                                    fit: BoxFit.cover,
                                    width: 1000.0,
                                  ),
                                ],
                              )
                          )
                      );
                    }).toList(),
                    autoPlay: false,
                    viewportFraction: 0.9,
                    aspectRatio: 2.0,
                  )
              ),
            ],
          );
  }
  Future<Null> getStatusListPoint() async {
    final response = await http.post(urlSet,body: {"con_no": ""},headers: {"apikey":apikey});
    final responseJson = json.decode(response.body);
    print("responseJson1::"+response.body);

    setState(() {
      _StatusListData.clear();
      for (Map user in responseJson) {
        _StatusListData.add(StatusList.fromJson(user));
      }

    });

  }
  Future<Null> getStatusListPointBox(String phone) async {
    final response = await http.post(urlSetBox,body: {"phoneno": phone},headers: {"apikey":apikey});
    final responseJson = json.decode(response.body.replaceAll("{", "[{").replaceAll("}", "\"}]").replaceAll(":", ":\"").replaceAll(",", "\","));
    print("responseJson1::"+response.body.replaceAll("{", "[{").replaceAll("}", "}]").replaceAll(":", ":\"").replaceAll(",", "\","));

    setState(() {
      _StatusListDataBox.clear();
      for (Map user in responseJson) {
        _StatusListDataBox.add(StatusListBoxO.fromJson(user));
      }

    });

  }
}