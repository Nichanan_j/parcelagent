import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:new_s_t/players.dart';
import 'package:new_s_t/Home/webview.dart';


class Autocomplete extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: AutoComplete(),
      ),
    );
  }
}

class AutoComplete extends StatefulWidget {
  @override
  _AutoCompleteState createState() => new _AutoCompleteState();
}

class _AutoCompleteState extends State<AutoComplete> {
  GlobalKey<AutoCompleteTextFieldState<Players>> key = new GlobalKey();

  AutoCompleteTextField searchTextField;

  TextEditingController controller = new TextEditingController();

  _AutoCompleteState();

  void _loadData() async {
    await PlayersViewModel.loadPlayers();
  }

  @override
  void initState() {
    _loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          title: Text('Auto Complete List Demo'),
          actions: <Widget>[
        new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () async {
              Navigator.of(context).pushAndRemoveUntil(
                  new MaterialPageRoute(
                      builder: (BuildContext context) => new MyAppHomePage()),
                  (Route<dynamic> route) => false);
            }),
      ]
        ),
        body: new Center(
            child: new Column(children: <Widget>[
          new Column(children: <Widget>[
            searchTextField = AutoCompleteTextField<Players>(
                style: new TextStyle(color: Colors.black, fontSize: 16.0),
                decoration: new InputDecoration(
                    suffixIcon: Container(
                      width: 85.0,
                      height: 60.0,
                    ),
                    contentPadding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
                    filled: false,
                    hintText: 'Search Player Name',
                    hintStyle: TextStyle(color: Colors.black)),
                itemSubmitted: (item) {
                  setState(() => searchTextField.textField.controller.text =
                      item.zipcode+' '+item.DISTRICT_NAME+' '+item.AMPHUR_NAME+' '+item.PROVINCE_NAME);
                },
                clearOnSubmit: false,
                key: key,
                suggestions: PlayersViewModel.players,
                itemBuilder: (context, item) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(item.zipcode,
                      style: TextStyle(
                        fontSize: 16.0
                      ),),
                      Padding(
                        padding: EdgeInsets.all(15.0),
                      ),
                      Text(item.DISTRICT_NAME,),
                      Text(item.AMPHUR_NAME,),
                      Text(item.PROVINCE_NAME,)
                    ],
                  );
                },
                itemSorter: (a, b) {
                  return a.zipcode.compareTo(b.zipcode);
                },
                itemFilter: (item, query) 
                {;
                  return item.zipcode
                      .toLowerCase()
                      .startsWith(query.toLowerCase());
                }),
          ]),
        ])));
  }
}
