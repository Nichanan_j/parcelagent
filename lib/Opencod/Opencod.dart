import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:new_s_t/Home/webview.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:new_s_t/players.dart';
// import 'package:firebase_core/firebase_core.dart';



class Confirm extends StatefulWidget {
  Confirm({Key key}) : super(key: key);
  @override
  _ConfirmState createState() => _ConfirmState();
}

class _ConfirmState extends State<Confirm> {
  String _mySelection;
  SharedPreferences sharedPreferences;
  final String url = "https://agent.whatitems.com/bank/name";

  List data = List();


  Future<String> getSWData() async {
    var res = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
    var resBody = json.decode(res.body);

    setState(() {
      data = resBody;
    });

    print(resBody);

    return "Sucess";
  }

  @override
  void initState() {
    super.initState();
    this.getSWData();
    _loadData();
  }

  final formKey = GlobalKey<FormState>();

  final address_detailTextFieldController = TextEditingController();
  final acc_nameTextFieldController = TextEditingController();
  final acc_noTextFieldController = TextEditingController();

  String address_detail = '';
  String acc_name = '';
  String acc_no = '';
  List<String> trans = [];
  String _myActivity;
  String autocomplete;
  GlobalKey<AutoCompleteTextFieldState<Players>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
  final controllerauto = TextEditingController();
  _ConfirmState();
  // _AutoCompleteState();
  void _loadData() async {
    sharedPreferences = await SharedPreferences.getInstance();
    await PlayersViewModel.loadPlayers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[20],
      appBar: AppBar(title: Text('เปิดใช้งาน COD'), actions: <Widget>[
        //  IconButton(
        //     icon: new Icon(Icons.arrow_back),
        //     onPressed: () async {

        //       Navigator.of(context).pushAndRemoveUntil(
        //           new MaterialPageRoute(
        //               builder: (BuildContext context) => new MyAppHomePage()),
        //           (Route<dynamic> route) => false);
        //     }),
      ]),
      body: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  left: 15.0, right: 10.0, top: 20.0, bottom: 90.0),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/s.jpg"),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 10.0, top: 90.0),
              child: address_detailTextField(),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 10.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  searchTextField = AutoCompleteTextField<Players>(
                      //style: new TextStyle(color: Colors.black, fontSize: 12.0),
                      decoration: new InputDecoration(
                        labelText: 'รหัสไปรษณีย์',
                        border: OutlineInputBorder(),
                        icon: new Icon(Icons.local_post_office),
                        suffixIcon: Container(
                          width: 85.0,
                          height: 60.0,
                        ),
                        contentPadding: EdgeInsets.only(
                            top: 30.0, bottom: 3.0, right: 10.0, left: 15.0),
                        filled: false,
                        //hintText: 'รหัสไปรษณีย์',
                        //hintStyle: TextStyle(color: Colors.black)
                      ),
                      controller: controllerauto,
                      itemSubmitted: (item) {
                        setState(() =>
                            searchTextField.textField.controller.text =
                                item.DISTRICT_NAME +
                                    ' ' +
                                    item.AMPHUR_NAME +
                                    ' ' +
                                    item.PROVINCE_NAME +
                                    ' ' +
                                    item.zipcode);
                      },
                      clearOnSubmit: false,
                      key: key,
                      suggestions: PlayersViewModel.players,
                      itemBuilder: (context, item) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              item.zipcode,
                              // style: TextStyle(
                              //   fontSize: 16.0
                              // ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10.0),
                            ),
                            Text(
                              item.DISTRICT_NAME,
                            ),
                            Text(
                              item.AMPHUR_NAME,
                            ),
                            Text(
                              item.PROVINCE_NAME,
                            )
                          ],
                        );
                      },
                      itemSorter: (a, b) {
                        return a.zipcode.compareTo(b.zipcode);
                      },
                      itemFilter: (item, query) {
                        
                        return item.zipcode
                            .toLowerCase()
                            .startsWith(query.toLowerCase());
                      }),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 10.0, top: 20.0),
              child: acc_nameTextField(),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 10.0, top: 20.0),
              child: acc_noTextField(),
            ),
            Container(
              margin: EdgeInsets.only(left: 15.0, right: 10.0, top: 20.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(1.0),
                border: Border.all(
                    color: Colors.grey, style: BorderStyle.solid, width: 0.80),
              ),
              child: new DropdownButton(
                items: data.map((item) {
                  return new DropdownMenuItem(
                    
                    child: new Text(item['bankTh']),
                    value: item['bankTh'].toString(),
                  );
                }).toList(),
                hint: Text('กรุณาเลือกธนาคาร'),
                onChanged: (newVal) {
                  setState(() {
                    _mySelection = newVal;
                  });
                },
                value: _mySelection,
              ),
            ),
            Container(
                margin: EdgeInsets.only(
                    top: 30.0, bottom: 3.0, right: 50.0, left: 50.0),
                alignment: Alignment(0.0, 0.0),
                child: saveButton(context)),
          ],
        ),
      ),
    );
  }

  void changedDropDownItem(String selectedVender) {
    setState(() {
      _mySelection;
      print(_mySelection);
    });
  }

  void uploadToServer() {
    print('You Click Upload');
    // formKey.currentState.reset();
    print(formKey.currentState.validate());
    formKey.currentState.save();
  }

  Widget address_detailTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'ที่อยู่',
        border: OutlineInputBorder(),
        icon: new Icon(Icons.home),
        
      ),
      controller: address_detailTextFieldController,
      validator: (String value) {
        if (value.length == 0) {
          return 'address_detail';
        } else {
          address_detail = null;
        }
      },
    );
  }

  Widget acc_nameTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'ชื่อบัญชี',
        border: OutlineInputBorder(),
        icon: new Icon(Icons.person),
      ),
      controller: acc_nameTextFieldController,
      validator: (String value) {
        if (value.length == 0) {
          return 'acc_name';
        } else {
          acc_name = null;
        }
      },
    );
  }

  Widget acc_noTextField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'เลขที่บัญชี',
        border: OutlineInputBorder(),
        
        icon: new Icon(Icons.confirmation_number),
      ),
      controller: acc_noTextFieldController,
      validator: (String value) {
        if (value.length == 0) {
          return 'acc_no';
        } else {
          acc_no = null;
        }
      },
    );
  }

  Widget saveButton(BuildContext context) {
    return RaisedButton(
      color: Colors.blue,
      child: Text(
        'Save',
        style: TextStyle(color: Colors.white),
      ),
      onPressed: () {
        this.acc_name = acc_nameTextFieldController.text.toString();
        this.address_detail = address_detailTextFieldController.text.toString();
        this.acc_no = acc_noTextFieldController.text.toString();
        this.autocomplete = controllerauto.text.toString();
        this.trans.add(this.acc_name);
        this.trans.add(this.address_detail);
        this.trans.add(this.acc_no);
        var xxx = {
          "memberId": sharedPreferences.getString("phonenumber"),
          "memberAddress": this.address_detail + this.autocomplete,
          "bankAccName": this.acc_name,
          "bankIssue": this._mySelection,
          "bankAcc": this.acc_no,
          "bankImg": "/images/test.png"
        };

        print("Data to send : ${json.encode(xxx)}");
        sendtoserver(xxx);
        setState(() {});
        print('save complete');
        acc_nameTextFieldController.clear();
        address_detailTextFieldController.clear();
      },
    );
  }
}

Future<String> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  print("Response Status Code = ${response.statusCode}");
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;
}

Future sendtoserver(data) async {
  print('======>_<======');
  print('rrrr');
  print('======>_<======');
  var url = 'https://apidev.whatitems.com/parcel/agent/update/cod/register/api';

  var response = await apiRequest(url, data);

  print('======>_<======');
  print('======>_<======');
  print(response);
  print('======>_<======');
  print('======>_<======');
}
