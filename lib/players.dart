import 'dart:convert';
import 'package:flutter/services.dart';

class Players {
  String keyword;
  int id;
  String zipcode;
  String PROVINCE_NAME;
  String DISTRICT_NAME;
  String AMPHUR_NAME;
  Players(
      {this.zipcode, this.AMPHUR_NAME, this.PROVINCE_NAME, this.DISTRICT_NAME});

  factory Players.fromJson(Map<String, dynamic> parsedJson) {
    return Players(
        zipcode: parsedJson['zipcode'] as String,
        AMPHUR_NAME: parsedJson['AMPHUR_NAME'] as String,
        PROVINCE_NAME: parsedJson['PROVINCE_NAME'] as String,
        DISTRICT_NAME: parsedJson['DISTRICT_NAME'] as String);
  }
}

class PlayersViewModel {
  static List<Players> players;

  static Future loadPlayers() async {
    try {
      players = new List<Players>();
      String jsonString = await rootBundle.loadString('assets/players.json');
      Map parsedJson = json.decode(jsonString);
      var categoryJson = parsedJson['players'] as List;
      for (int i = 0; i < categoryJson.length; i++) {
        players.add(new Players.fromJson(categoryJson[i]));
      }
    } catch (e) {
      print(e);
    }
  }
}
